#include "qmltermwidget_plugin.h"

#include "TerminalDisplay.h"
#include "TerminalSession.h"

#include <qqml.h>
#include <QQmlEngine>
#include <QDir>

using namespace Konsole;

void QmltermwidgetPlugin::registerTypes(const char *uri)
{
    // @uri org.kde.konsoleqml
    qmlRegisterType<TerminalDisplay>(uri, 1, 0, "TerminalEmulator");
    qmlRegisterType<TerminalSession>(uri, 1, 0, "TerminalSession");
}

void QmltermwidgetPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    QQmlExtensionPlugin::initializeEngine(engine, uri);

    QStringList pwds = engine->importPathList();
}
